def main():
	inputNum = 0
	credits = 0
	creditX = 0
	status = "Freshmen"
	while creditX != 0 or inputNum == 0:
		creditX = int(input("Please enter credits (ending with 0): "))
		credits += creditX
		inputNum += 1
	if credits >= 7:
		status = "Sophomore"
	if credits >= 16:
		status = "Junior"
	if credits >= 26:
		status = "Senior"
	print("The student is a {0}".format(status))
main()
