from random import random
def simOneGame(probA, probB, serving):
	# Simulates a single game or racquetball between players whose
	# abilities are represented by the probability of winning a serve.
	# Returns final scores for A and B
	scoreA = 0
	scoreB = 0
	while not gameOver(scoreA, scoreB):
		if serving == "A" :
			if random() < probA:
				scoreA = scoreA + 1 
			else:
				serving = "B"
		else:
			if random() < probB:
				scoreB = scoreB + 1
			else:
				serving = "A"
	return scoreA, scoreB
def gameOver(a, b):
     # a and b represent scores for a racquetball game
     # Returns True if the game is over, False otherwise.
     return a== 15 or b== 15
def printSummary(winsA, winsB):
     # Prints a summary of wins for each player.
     n = winsA + winsB
     print("\nGames simulated: ", n)
     print("Wins for A: {0} ({1: 0.1%})".format(winsA, winsA/n))
     print("Wins for B: {0} ({1: 0.1%})".format(winsB, winsB/n))
def main():
	aWins = 0
	bWins = 0
	aShutouts = 0
	bShutouts = 0
	gameNum = 0
	aProb = float(input("What is the probability of A winning a serve: "))
	bProb = float(input("What is the probability of B winning a serve: "))
	winNum = int(input("How many games must the winner win?: "))
	while aWins != winNum and bWins != winNum:
		gameNum = gameNum + 1
		if gameNum %2 == 0:
			serving = "A"
		else:
			serving = "B"
		scoreA,scoreB = simOneGame(aProb,bProb,serving)
		if scoreA == 15:
			aWins = aWins + 1
			if scoreB == 0:
				aShutouts = aShutouts + 1
		else:
			if scoreA == 0:
				bShutouts = bShutouts + 1
			bWins = bWins + 1
	if aWins == winNum:
		print("A wins")
	else:
		print("B wins")
	print("A won {0} times, B won {1} times".format(aWins,bWins))
	games = aWins + bWins
	print("A's win percentage was {0}, B's win percentage was {1}".format((aWins/games),(bWins/games)))
	if aWins == 0:
		aWins = 1
	if bWins == 0:
		bWins = 1
	print("A's shutout to win percentage was {0}, B's shutout to win percentage was {1}".format((aShutouts/aWins),(bShutouts/bWins)))
	print("A had {0} shutouts, B had {1} shutouts".format(aShutouts,bShutouts))
main()
