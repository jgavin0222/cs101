from random import random
def simOneGame(probA, probB, serving):
	# Simulates a single game or racquetball between players whose
	# abilities are represented by the probability of winning a serve.
	# Returns final scores for A and B
	scoreA = 0
	scoreB = 0
	while not gameOver(scoreA, scoreB):
		if serving == "A" :
			if random() < probA:
				scoreA = scoreA + 1 
			else:
				serving = "B"
		else:
			if random() < probB:
				scoreB = scoreB + 1
			else:
				serving = "A"
	return scoreA, scoreB
def gameOver(a, b):
	# a and b represent scores for a racquetball game
	# Returns True if the game is over, False otherwise.
	if a > 14 and b <= a-2:
		return True
	if b > 14 and a <= b-2:
		return True
	return False
def printSummary(winsA, winsB):
	# Prints a summary of wins for each player.
	n = winsA + winsB
	print("\nGames simulated: ", n)
	print("Wins for A: {0} ({1: 0.1%})".format(winsA, winsA/n))
	print("Wins for B: {0} ({1: 0.1%})".format(winsB, winsB/n))
def main():
	aWins = 0
	bWins = 0
	gameNum = 0
	aProb = float(input("What is the probability of A winning a serve: "))
	bProb = float(input("What is the probability of B winning a serve: "))
	winNum = int(input("How many games must the winner win?: "))
	while aWins != winNum and bWins != winNum:
		gameNum = gameNum + 1
		if gameNum %2 == 0:
			serving = "A"
		else:
			serving = "B"
		scoreA,scoreB = simOneGame(aProb,bProb,serving)
		if scoreA == 15:
			aWins = aWins + 1
		else:
			bWins = bWins + 1
	if aWins == winNum:
		print("A wins")
	else:
		print("B wins")
main()
