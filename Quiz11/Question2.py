def main():
	array = []
	while True:
		value = int(input("Please enter a value to add to the list (-1 to end): "))
		if value == -1:
			break
		else:
			array.append(value)
	print(removeDuplicates(array))
def removeDuplicates(somelist):
	for i in range(0,len(somelist)-1):
		for j in range(0,i):
			if i != j:
				if somelist[i] == somelist[j]:
					del somelist[j]
	return somelist
main()
