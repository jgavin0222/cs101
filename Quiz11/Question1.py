import random
def main():
	value = 0
	array = []
	while True:
		value = int(input("Please enter a value, -1 to stop: "))
		if value == -1:
			break
		else:
			array.append(value)
	shuffled = shuffle(array)
	print(shuffled)
def shuffle(myList):
	for i in range(0,100):
		randA = random.randint(1,len(myList))
		randB = random.randint(1,len(myList))
		while randA == randB:
			randB = random.randint(1,len(myList))
		myList = flip(myList,randA,randB)
	return myList
def flip(myList,i,j):
	i = i - 1
	j = j - 1
	a = myList[i]
	b = myList[j]
	myList[i] = b
	myList[j] = a
	return myList	
main()
