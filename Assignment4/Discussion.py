from graphics import *
def main():
	win = GraphWin()
	shape = Circle(Point(50,50),20)
	shape.setOutline("red")
	shape.setFill("red")
	shape.draw(win)
	while true:
		p = win.getMouse()
		c = shape.getCenter()
		dx = p.getX() - c.getX()
		dy = p.getY() - c.getY()
		shape.move(dx,dy)
	win.close()
main()
