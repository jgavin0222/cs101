from graphics import *
def main():
	win = GraphWin()
	shape = Rectangle(Point(50,50),Point(50+50,50+50))
	shape.setOutline("red")
	shape.setFill("red")
	shape.draw(win)
	for i in range(10):
		p = win.getMouse()
		shape2 = Rectangle(Point(p.getX()-25,p.getY()-25),Point(p.getX()+25,p.getY()+25))
		shape2.setFill("red")
		shape2.setOutline("red")
		shape2.draw(win)
	print("Click again to exit")
	win.getMouse()
	win.close()
main()
