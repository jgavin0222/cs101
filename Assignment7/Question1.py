def main():
	hours = float(input("Enter the number of hours worked: "))
	rate = float(input("Enter the hourly wage: "))
	overtime = 0
	if hours > 40:
		overtime = hours - 40
		hours = 40
	pay = (hours * rate) + (overtime * (rate+(rate/2)))
	print("Pay: {0}".format(pay))
main()
