def main():
	value = int(input("Please enter a score: "))
	grade = "F"
	if value > 59:
		grade = "D"
	if value > 69:
		grade = "C"
	if value > 79:
		grade = "B"
	if value > 89:
		grade = "A"
	print("Grade: {0}".format(grade))
main()
