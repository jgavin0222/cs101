def main():
	value = int(input("Please enter a score: "))
	grade = "F"
	if value == 5:
		grade = "A"
	elif value == 4:
		grade = "B"
	elif value == 3:
		grade = "C"
	elif value == 2:
		grade = "D"
	print("Grade: {0}".format(grade))
main()
