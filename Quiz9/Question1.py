from random import random
def main():
	probA,probB = getProbabilities()
	games = int(input("How many games are to be played: "))
	aWins,bWins = simGames(games,probA,probB)
	output(games,aWins,bWins)
def simGames(games,probA,probB):
	aWins = 0
	bWins = 0
	for i in range(0,games):
		sA,sB = simGame(probA,probB)
		if sA == 25:
			aWins = aWins + 1
		else:
			bWins = bWins + 1
	return aWins,bWins
def simGame(probA,probB):
	aPoints = 0
	bPoints = 0
	while not gameOver(aPoints,bPoints):
		if random()<probA:
			aPoints = aPoints + 1
		else:
			bPoints = bPoints + 1
	return aPoints,bPoints
def gameOver(aPoints,bPoints):
	if aPoints == 25 or bPoints == 25:
		return True
	return False
def getProbabilities():
	probA = float(input("What is the probability team A wins a rally: "))
	probB = 1-probA
	return probA,probB	
def output(games,aWins,bWins):
	print("There were {0} games played".format(games))
	print("Team A won {0} games, Team B won {1} games".format(aWins,bWins))
	print("Team A won {0} percent of the games".format(aWins/games))
	print("Team B won {0} percent of the games".format(bWins/games))
main()
