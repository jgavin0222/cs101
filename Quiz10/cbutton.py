# button.py
from graphics import *
import math

class CButton:

    """A button is a labeled rectangle in a window.
    It is activated or deactivated with the activate()
    and deactivate() methods. The clicked(p) method
    returns true if the button is active and p is inside it."""

    def __init__(self, win, p1, radius, label):
        self.p1 = p1
        self.radius = radius
        self.rect = Circle(p1,radius)
        self.rect.setFill('lightgray')
        self.rect.draw(win)
        self.label = Text(p1, label)
        self.label.draw(win)
        self.deactivate()

    def clicked(self, p):
        x = p.getX()
        y = p.getY()
        centerX = self.p1.getX()
        centerY = self.p1.getY()
        distance = math.sqrt(((centerX-x)**2)+((centerY-y)**2))
        if distance <= self.radius:
            return self.activate
		

    def getLabel(self):
        "Returns the label string of this button."
        return self.label.getText()

    def activate(self):
        "Sets this button to 'active'."
        self.label.setFill('black')
        self.rect.setWidth(2)
        self.active = True

    def deactivate(self):
        "Sets this button to 'inactive'."
        self.label.setFill('darkgrey')
        self.rect.setWidth(1)
        self.active = False
