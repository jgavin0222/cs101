from graphics import *
class Button:
	def __init__(self , win , center , width , height , label):
		w,h = width/2.0,height/2.0
		x,y = center.getX(), center.getY()
		self.xmax, self.xmin = x+w, x-w
		self.ymax, self.ymin = y+h, y-h
		p1 = Point(self.xmin, self.ymin)
		p2 = Point(self.xmax, self.ymax)
		self.rect = Rectangle(p1,p2)
		self.rect.setFill('lightgray')
		self.rect.draw(win)
		self.label = Text(center, label)
		self.label.draw(win)
		self.deactivate()
	def clicked(self, p):
		return (self.active and
			self.xmin <= p.getX() <= self.xmax and
			self.ymin <= p.getY() <= self.ymax)
	def getLabel(self):
		return self.label.getText()
	def activate(self):
		self.label.setFill('black')
		self.rect.setWidth(2)
		self.active = True
	def deactivate(self):
		self.label.setFill('darkgrey')
		self.rect.setWidth(1)
		self.active = False
class GUI:
	def __init__(self):
		self.win = win = GraphWin("GUI",500,500)
		win.setCoords(0,4.5,4,.5)
		Text(Point(1,1),"Value").draw(win)
		Text(Point(1,2),"Factorial").draw(win)
		self.result = Text(Point(2,2),"").draw(win)
		self.input = Entry(Point(2,1),5).draw(win)
		self.run = Button(win,Point(1,4),1.25,.5,"Run")
		self.quit = Button(win,Point(3,4),1.25,.5,"Exit")
		self.run.activate()
		self.quit.activate()
	def interact(self):
		while True:
			pt = self.win.getMouse()
			if self.run.clicked(pt):
				self.calculate()
			if self.quit.clicked(pt):
				return
	def calculate(self):
		value = int(self.input.getText())
		for i in range(1,value):
			value = value * i
		self.result.setText(value)
	def close(self):
		self.win.close()
def main():
	g = GUI()
	g.interact()		
main()
