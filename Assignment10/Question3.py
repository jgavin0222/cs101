from graphics import *
import random
class Button:
	def __init__(self , win , center , width , height , label):
		w,h = width/2.0,height/2.0
		x,y = center.getX(), center.getY()
		self.xmax, self.xmin = x+w, x-w
		self.ymax, self.ymin = y+h, y-h
		p1 = Point(self.xmin, self.ymin)
		p2 = Point(self.xmax, self.ymax)
		self.rect = Rectangle(p1,p2)
		self.rect.setFill('lightgray')
		self.rect.draw(win)
		self.label = Text(center, label)
		self.label.draw(win)
		self.deactivate()
	def clicked(self, p):
		return (self.active and
			self.xmin <= p.getX() <= self.xmax and
			self.ymin <= p.getY() <= self.ymax)
	def getLabel(self):
		return self.label.getText()
	def activate(self):
		self.label.setFill('black')
		self.rect.setWidth(2)
		self.active = True
	def deactivate(self):
		self.label.setFill('darkgrey')
		self.rect.setWidth(1)
		self.active = False
class GUI:
	def __init__(self):
		self.val = random.randint(1,3)
		self.win = win = GraphWin("GUI",1000,200)
		win.setCoords(0,4.5,4,.5)
		self.d1 = Button(win,Point(0.5,1),1,.5,"Door 1")
		self.d2 = Button(win,Point(2,1),1,.5,"Door 2")
		self.d3 = Button(win,Point(3.5,1),1,.5,"Door 3")
		self.t1 = Text(Point(2,2),"Please Select a Door").draw(win)
		self.d1.activate()
		self.d2.activate()
		self.d3.activate()
	def interact(self):
		while True:
			pt = self.win.getMouse()
			if self.d1.clicked(pt):
				if self.val == 1:
					self.won()
				else:
					self.lose()
			if self.d2.clicked(pt):
				if self.val == 2:
					self.won()
				else:
					self.lose()
			if self.d3.clicked(pt):
				if self.val == 3:
					self.won()
				else:
					self.lose()
	def won(self):
		self.t1.setText("Congratulations, you won")
	def lose(self):
		self.t1.setText("Sorry, you lost, the correct door was {0}".format(self.val))
				
def main():
	g = GUI()
	g.interact()
main()
