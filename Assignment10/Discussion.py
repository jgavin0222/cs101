import random
def main():
	lower = int(input("Enter the minimum value that can be generated: "))
	upper = int(input("Enter the maximum value that can be generated: "))
	ng = NumberGenerator(lower,upper)
	print("The current number is {0}".format(ng.getVal()))
	ng.newVal()
	print("The new number is {0}".format(ng.getVal()))
class NumberGenerator:
	def __init__(self,minimum,maximum):
		self.min = minimum
		self.max = maximum
		self.val = random.randint(minimum,maximum)
	def getVal(self):
		return self.val
	def newVal(self):
		self.val = random.randint(self.min,self.max)
main()
