from graphics import *
def main():
	win = GraphWin()
	c = Circle(Point(50,50),50)
	e1 = Point(30,30)
	e2 = Point(70,30)
	p = Polygon(Point(30,70),Point(45,80),Point(55,80),Point(70,70))	

	p.draw(win)
	c.draw(win)
	e1.draw(win)
	e2.draw(win)

	c2 = Circle(Point(50,150),50)
	e3 = Point(30,130)
	e4 = Point(70,130)
	p2 = Polygon(Point(30,180),Point(45,170),Point(55,170),Point(70,180))	

	p2.draw(win)
	c2.draw(win)
	e3.draw(win)
	e4.draw(win)
	
	input("Press enter to close")
	win.close()
main()
