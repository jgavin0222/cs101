from graphics import *
def main():
	win = GraphWin()
	r = Rectangle(Point(10,10),Point(100,250))
	
	c1 = Circle(Point(55,50),25)
	c2 = Circle(Point(55,110),25)
	c3 = Circle(Point(55,170),25)
	
	r.setFill("black")
	c1.setFill("red")
	c2.setFill("yellow")
	c3.setFill("green")
	
	r.draw(win)
	c1.draw(win)
	c2.draw(win)
	c3.draw(win)

	input("Press enter to close")
	win.close()
main()
