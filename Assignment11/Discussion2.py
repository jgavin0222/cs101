def main():
	s1 = [2,1,4,3]
	s2 = ['c','a','b']
	
	s1a = s1.copy()
	s1b = s1.copy()
	s1c = s1.copy()
	s1d = s1.copy()

	s1a.remove(2)
	s1b.sort()
	s1c.append([s2.index('b')])

	print(s1a)
	print(s1b)
	print(s1c)

	s2a = s2.copy()
	s2b = s2.copy()
	
	#s2a.pop(s1d.pop(2))
	s2b.insert(s1[0],'d')

	print("Omitting s2.pop(s1.pop(2)), out of range error")
	print(s2b)
main()
