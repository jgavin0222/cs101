def main():
	list = getList()
	max = getMax(list)
	print("The maximum number is {0}".format(max))
def getList():
	list = []
	while True:
		value = int(input("Please enter a value (0 to end): "))
		if value == 0:
			break
		list.append(value)
	return list
def getMax(list):
	max = list[0]
	for i in range(0,len(list)):
		if list[i] > max:
			max = list[i]
	return max
main()
