import math
def main():
	r = float(input("Please enter an investment rate: "))
	i = 1
	ix = i*2
	t = 1
	while i < ix:
		i += i*r
		t += 1
	print("The time it takes to double at this rate is {0}".format(t))
main()
