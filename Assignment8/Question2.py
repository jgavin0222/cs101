def main():
	print("        ",end="")
	for i in range(5,51,5):
		print("{0}".format(i),end="     ")
	for i in range(-20,61,10):
		print("\n{0:3.0f}".format(i),end="")
		for j in range(5,51,5):
			print("{0:7.0f}".format(windChill(i,j)),end="")
	print()
def windChill(T,V):
	return (35.74 + 0.6215*T - 35.75*(V**0.16)+0.4275*T*(V**0.16))
main()
