def main():
	n = int(input("Enter the first number of odd values to sum: "))
	x = 1
	sum = 0
	while x<=n:
		sum += 2*x - 1
		x += 1
	print("The sum is {0}".format(sum))
main()
