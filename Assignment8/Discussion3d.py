def main():
	n = int(input("Please enter a whole number: "))
	count = 0
	while n != 1:
		n = n/2
		count += 1
	print("The number of times the number can be divided by 2 is {0}".format(count))
main()
