def main():
	n = int(input("Enter a number to sum until: "))
	x = 1
	sum = 0
	while x <= n:
		sum += x
		x += 1
	print("The sum is {0}".format(sum))
main()	
