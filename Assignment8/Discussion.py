import math
def main():
	repeat = int(input("How many spheres do you have: "))
	for i in range(repeat):
		options()
def options():
	decision = int(input("Enter 1 for volume enter 2 for area: "))
	radius = float(input("Please enter a sphere radius: "))
	if decision == 1:
		print("Volume: {0}".format(sphereVolume(radius)))
	if decision == 2:
		print("Area: {0}".format(sphereArea(radius)))
def sphereVolume(radius):
	return (4/3) * math.pi * radius * radius * radius
def sphereArea(radius):
	return 4 * math.pi * radius * radius
main()
