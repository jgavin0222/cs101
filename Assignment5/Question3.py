def main():
	score = int(input("Please enter a exam grade: "))
	print("The letter grade is {0}".format(examLetterGrade(score)))
def examLetterGrade(score):
	if score >= 90:
		return "A"
	if score >= 80:
		return "B"
	if score >= 70:
		return "C"
	if score >= 60:
		return "D"
	return "F"
main()
