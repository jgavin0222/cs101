for ch in "aardvark":
	print(ch)
print("Split")
for w in "Now is the winter of our discontent...".split():
	print(w)
print("Split")
for w in "Mississippi".split("i"):
	print(w,end=" ")
print("Split")
msg=""
for s in "secret".split("e"):
	msg=msg+s
print(msg)
print("Split")
msg=""
for ch in "secret":
	msg = msg + chr(ord(ch)+1)
print(msg)
