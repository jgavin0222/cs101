# convert.py
#	A proram to convert Celsius temps to Fahrenheit
# by: Susan Computewell
# Modified by Jack Gavin for CS101 Assignment 2 (Question 1)

def main():
	print("This program converts Celsius temperatures to Fahrenheit")
	celsius = eval(input("What is the Celsius temperature: "))
	fahrenheit = 9/5 * celsius + 32
	print("The temperature is", fahrenheit, "degrees Fahrenheit.")
main()
