# convert.py
#	A proram to convert Celsius temps to Fahrenheit
# by: Susan Computewell
# Modified by Jack Gavin for CS101 Assignment 2 (Question 3)

def getTemp():
	celsius = eval(input("What is the Celsius temperature: "))
	fahrenheit = 9/5 * celsius + 32
	print("The temperature is", fahrenheit, "degrees Fahrenheit.")

def main():
	for i in range(5):
		getTemp()
main()
