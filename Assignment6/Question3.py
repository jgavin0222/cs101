import math
def main():
	radius = float(input("Enter a radius: "))
	print("Area: {0}".format(sphereArea(radius)))
	print("Volume: {0}".format(sphereVolume(radius)))
def sphereArea(radius):
	return 4 * math.pi * radius * radius
def sphereVolume(radius):
	return (4/3) * math.pi * radius * radius * radius
main()
