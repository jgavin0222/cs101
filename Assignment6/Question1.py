def main():
	lyrics("cow","moo")
	lyrics("pig","oink")
	lyrics("duck","quack")
	lyrics("horse","neigh")
	lyrics("lamb","baa")
def lyrics(animal,noise):
	print("Old MacDonald had a farm, Ee-igh, Ee-igh, Oh!")
	print("And on that farm he had a {0}, Ee-igh, Ee-igh, Oh!".format(animal))
	print("With a {0}, {0} here and a {0}, {0} there.".format(noise))
	print("Here a {0}, there a {0}, everywhere a {0}, {0}.".format(noise))
	print("Old MacDonald had a farm, Ee-igh, Ee-igh, Oh!")
main()
